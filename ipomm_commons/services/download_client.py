import os
import aiofiles
import aiohttp
from fastapi import Depends
from fastapi_extra.utils.aiohttp_client import SessionClientAiohttp

from fastapi_extra.service.basic_service_client import BasicServiceClient

chunk_size = 1024


class DownloadClient(BasicServiceClient):

    def __init__(self, httpClient: aiohttp.ClientSession = Depends(
            SessionClientAiohttp(tenant_forward=False, enable_ssl=False))):
        super().__init__(httpClient)

    async def download_file(self, url, path, progressService):
        raise NotImplemented()
        async with self.httpClient.get(url) as response:
            if response.status != 200:
                raise Exception("Cannot download file")
            if "content-size" in response.headers:
                content_size = int(response.headers["content-size"])
                progressService.next_step("Downloading file")
            elif "Content-Length" in response.headers:
                content_size = int(response.headers["Content-Length"])
                progressService.next_step("Downloading file")
            else:
                content_size = None
                progressService.next_step("Downloading file")
            try:
                async with aiofiles.open(path, 'wb') as afp:
                    total = 0
                    while True:
                        chunk = await response.content.read(chunk_size)

                        total += len(chunk)
                        if content_size:
                            progressService.set_progress(total / content_size)

                        if not chunk:
                            break
                        await afp.write(chunk)
                    return await response.release()
            except:
                os.remove(path)
