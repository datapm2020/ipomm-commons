import json
import logging
import os

import aiofiles
import aiohttp
from aiohttp import InvalidURL
from fastapi import HTTPException, Depends
from fastapi_extra.utils.aiohttp_client import SessionClientAiohttp

from app.utils import settings
from fastapi_extra.service.basic_service_client import BasicServiceClient

chunk_size = 1024

logger = logging.getLogger("File service")

class FileServiceClient(BasicServiceClient):
    url = settings.FILE_SERVICE_URL
    uri_getinfo = '/file-service/api/v1/files/'

    def __init__(self, httpClient: aiohttp.ClientSession = Depends(
        SessionClientAiohttp(tenant_forward=True, enable_ssl=True))):
        super().__init__(httpClient)

    async def download_file(self, file_id, path, callback=None):
        async with self.httpClient.get(self.url + f"/file-service/api/v1/files/{file_id}/download") as response:
            if response.status != 200:
                raise Exception("File not found")

            content_size = int(response.headers["content-size"])
            try:
                async with aiofiles.open(path, 'wb') as afp:
                    total = 0
                    while True:
                        chunk = await response.content.read(chunk_size)
                        total += len(chunk)
                        if callback:
                            callback(total / content_size)
                        if not chunk:
                            break
                        await afp.write(chunk)
                    return await response.release()
            except:
                os.remove(path)

    async def get_file_info(self, file_id):
        try:
            async with self.httpClient.get(self.url + self.uri_getinfo + file_id) as response:
                text = await response.text()
                result = json.loads(text)
                if 'result' not in result:
                    logger.error('File-service: ' + text)
                return result["result"]
        except InvalidURL as e:
            raise HTTPException(status_code=400, detail="The Url you provided is invalid")

