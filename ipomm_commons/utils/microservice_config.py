import os
from typing import List, Union

from pydantic import BaseSettings, validator


class MicroserviceConfig(BaseSettings):
    PROJECT_NAME: str
    URI_DEFAULT_PREFIX: str

    BACKEND_CORS_ORIGINS: List[str] = None

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    API_URL_PREFIX: str = None

    @validator("API_URL_PREFIX", pre=True)
    def api_url_prefix(cls, v: str, values, **kwargs) -> str:
        if v is not None:
            raise ValueError("Do not set api url it will be generated from default url suffix")
        return values["URI_DEFAULT_PREFIX"] + "/api"

    class Config:
        case_sensitive = True


if os.path.isdir('/secrets'):
    microservice_config = MicroserviceConfig(_secrets_dir="/secrets")
else:
    microservice_config = MicroserviceConfig()
