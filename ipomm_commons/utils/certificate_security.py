from fastapi import Depends

from fastapi_extra.schemas.certificate import Certificate

from fastapi_extra.utils.certificate_authentication import CertificateAuthentication

get_certificate = CertificateAuthentication(auto_error=True)


def verify_certificate(certificate: Certificate = Depends(get_certificate)):
    return certificate


try_get_certificate = CertificateAuthentication(auto_error=False)
