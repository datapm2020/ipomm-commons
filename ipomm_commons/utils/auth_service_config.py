import json
from typing import Any, Dict, Optional, Union

import requests
from fastapi import HTTPException
from pydantic import BaseSettings, validator


class AuthServiceSettings(BaseSettings):
    AUTH_API: str
    AUTH_SERVICE_URL: str = None
    JWT_BLACKLIST_ENABLED: bool = False
    JWT_ALGORITHM: str = "RS256"


    JWT_PUBLIC_KEY: Optional[str]

    @validator("JWT_PUBLIC_KEY", pre=True)
    def set_public_key(cls, v: str, values: Dict[str, Any]) -> Union[str, bytes]:
        if v is not None and v != "":
            return str.encode(v)
        else:
            try:
                if values["AUTH_SERVICE_URL"]==None:
                    raise Exception("You should specify AUTH_SERVICE_URL in the environment in order to get the public key")
                result = requests.get(values["AUTH_SERVICE_URL"] + "/auth-service/api/v1/public-key", verify=False).text
                return str.encode(json.loads(result)['result']['public-key'])
            except Exception:
                raise HTTPException(status_code=408, detail="Problem in Auth-service")

    class Config:
        case_sensitive = True


auth_service_settings = AuthServiceSettings()
