from typing import Union

from fastapi import Request, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import jwt
from pydantic import ValidationError
from starlette import status

from fastapi_extra.celery import celery_enabled
from ipomm_commons import repositories

from ipomm_commons.schemas import TokenPayload
from ipomm_commons.utils.auth_service_config import auth_service_settings


def verify_token_not_blacklisted(token_id: str) -> bool:
    if not auth_service_settings.JWT_BLACKLIST_ENABLED:
        return True
    if repositories.blacklistedToken.find_by_jti(jti=token_id):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Token has been revoked"
        )
    return True


class OAuth2UserPassword(OAuth2PasswordBearer):
    def __init__(
            self,
            tokenUrl: str,
            scheme_name: str = None,
            scopes: dict = None,
            auto_error: bool = True,
    ):
        super().__init__(tokenUrl,
                         scheme_name,
                         scopes,
                         auto_error)

    async def __call__(self, request: Request, security_scopes: SecurityScopes = None) -> Union[None, TokenPayload]:
        if "auth_token_data" in request.scope:
            if self.auto_error and request.scope['auth_token_data'] is None:
                raise request.scope['auth_token_exception']
            return request.scope['auth_token_data']

        token = await super().__call__(request)
        try:
            if not token:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED, detail="No token was passed"
                )
            else:
                try:
                    payload = jwt.decode(
                        token, auth_service_settings.JWT_PUBLIC_KEY, algorithms=[auth_service_settings.JWT_ALGORITHM]
                    )
                    token_data = TokenPayload(**payload)
                    if security_scopes:
                        for scope in security_scopes.scopes:
                            if scope not in token_data.scopes:
                                raise HTTPException(
                                    status_code=status.HTTP_403_FORBIDDEN, detail="Not enough permissions"
                                )
                except jwt.ExpiredSignatureError:
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED, detail="Token has expired"
                    )
                except (jwt.JWTError, ValidationError):
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED, detail="Could not validate Token"
                    )
                if token_data.token_type != "access":
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED,
                        detail="{0}_token is different from expected token type: Access".format(
                            token_data.token_type
                        ),
                    )

                verify_token_not_blacklisted(token_data.jti)
                # if token_data.tenant != request.state.tenantId:
                #     raise HTTPException(
                #         status_code=status.HTTP_401_UNAUTHORIZED,
                #         detail="Could not validate Token",
                #     )
                request.scope['auth_token_data'] = token_data
                return token_data
        except Exception as e:
            request.scope['auth_token_exception'] = e
            request.scope['auth_token_data'] = None
            if self.auto_error:
                raise e
            return None


verify_access_token = OAuth2UserPassword(tokenUrl=auth_service_settings.AUTH_API)

try_verify_access_token = OAuth2UserPassword(tokenUrl=auth_service_settings.AUTH_API, auto_error=False)


def try_get_user_id(token_data: Union[None, TokenPayload] = Depends(try_verify_access_token)) -> Union[str, None]:
    if not token_data:
        return None
    return token_data.sub


def get_user_id(token_data: Union[None, TokenPayload] = Depends(verify_access_token)) -> Union[str, None]:
    return token_data.sub


if celery_enabled:
    from fastapi_extra.celery.core.celery_job_params import CeleryParamsInjector
    CeleryParamsInjector.injection()(get_user_id)
    CeleryParamsInjector.injection()(try_get_user_id)


def get_auth_exception(request: Request):
    return request.scope.get('auth_token_exception', None)
