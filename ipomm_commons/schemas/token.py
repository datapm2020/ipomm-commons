import uuid
from datetime import datetime
from typing import Optional, List

from fastapi_extra.schemas.common import generate_unique_id
from pydantic import BaseModel, Field
from typing_extensions import Literal


class Token(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class TokenPayload(BaseModel):
    exp: datetime
    jti: str = Field(default_factory=generate_unique_id)
    iat: datetime = Field(default_factory=datetime.utcnow)
    token_type: Literal["access", "refresh"]
    sub: Optional[str] = None
    tenant: str
    lang: str
    scopes: Optional[List[str]]
