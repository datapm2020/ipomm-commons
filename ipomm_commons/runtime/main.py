import logging

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.types import ASGIApp

from fastapi_extra.runtime import fastapi_event
from fastapi_extra.runtime.fastapi_event import startup, shutdown
from ipomm_commons.utils import microservice_config

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

mounted_path = []


class CustomFastApi(FastAPI):

    def mount(self, path: str, app: ASGIApp, name: str = None) -> None:
        if path in mounted_path:
            logger.warning(f"A mounted app with path {path} this app will be ignored")
            return
        mounted_path.append(path)
        super().mount(path, app, name)


fastapi_app = CustomFastApi(on_startup=startup, on_shutdown=shutdown)

fastapi_event.on_startup = fastapi_app.on_event("startup")

fastapi_event.on_shutdown = fastapi_app.on_event("shutdown")

if microservice_config.BACKEND_CORS_ORIGINS:
    fastapi_app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in microservice_config.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )


@fastapi_app.get("/healthcheck")
async def status():
    return {"status": "available"}


class HealthCheckFilter(logging.Filter):
    def filter(self, record):
        return 'GET /healthcheck' not in record.getMessage()


logger = logging.getLogger("uvicorn.access")
logger.addFilter(HealthCheckFilter())
