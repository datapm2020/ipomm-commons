import importlib
import os

from ipomm_commons.runtime import fastapi_app

importing = False


def import_dynamic_apps(directory: str):
    global importing
    if importing == directory:
        return
    importing = directory
    if os.path.exists(directory):
        for filename in os.listdir(directory):
            f = os.path.join(directory, filename)
            if os.path.isdir(f):
                module = importlib.import_module(f"{directory}.{filename}")
                if module.api_version != filename.replace("_","."):
                    raise Exception(f"filename {filename} and version {module.api_version} do not correspond")
                exec(f"import {directory}.{filename}.main")
    importing = False
